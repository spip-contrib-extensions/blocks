<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Blocks
 *
 * @plugin     Blocks
 * @copyright  2023
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Blocks\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation et de mise à jour du plugin Blocks.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
 **/
function blocks_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];

	$maj['create'] = [
		['maj_tables', ['spip_blocks', 'spip_blocktypes', 'spip_blocktypes_liens']],
		['blocks_installe_config'],
	];

	$maj['1.1.0'] = [
		['blocks_update_1_0_1'],
		['maj_tables', ['spip_blocks', 'spip_blocks_liens', 'spip_blocktypes']],
	];

	$maj['1.2.0'] = [
		['maj_tables', ['spip_blocks', 'spip_blocktypes', 'spip_blocktypes_liens']],
	];

	$maj['1.3.0'] = [
		['maj_tables', ['spip_blocks', 'spip_blocktypes', 'spip_blocktypes_liens']],
		['blocks_update_1_3_0'],
		['sql_drop_table', 'spip_blocks_liens'],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin Blocks.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 **/
function blocks_vider_tables($nom_meta_base_version) {

	sql_drop_table('spip_blocks');
	sql_drop_table('spip_blocks_liens');
	sql_drop_table('spip_blocktypes');

	# Nettoyer les liens courants (le génie optimiser_base_disparus se chargera de nettoyer toutes les tables de liens)
	sql_delete('spip_documents_liens', sql_in('objet', ['block', 'blocktype']));
	sql_delete('spip_mots_liens', sql_in('objet', ['block', 'blocktype']));
	sql_delete('spip_auteurs_liens', sql_in('objet', ['block', 'blocktype']));
	# Nettoyer les versionnages et forums
	sql_delete('spip_versions', sql_in('objet', ['block', 'blocktype']));
	sql_delete('spip_versions_fragments', sql_in('objet', ['block', 'blocktype']));
	sql_delete('spip_forum', sql_in('objet', ['block', 'blocktype']));

	effacer_meta($nom_meta_base_version);
}

function blocks_installe_config() {
	include_spip('blocks_fonctions');

	// associer par défaut aux articles
	ecrire_config('blocks/objets',
		[
			0 => 'spip_articles',
		],
	);

	// un type de block de démo : titre + texte
	sql_insertq(
		'spip_blocktypes',
		[
			'titre'       => 'Démo texte simple',
			'identifiant' => 'demo_simple',
			'saisies'     => blocks_serialize([
				[
					'options'     =>
						[
							'nom'             => 'titre',
							'label'           => 'Titre',
							'conteneur_class' => 'pleine_largeur',
							'obligatoire'     => 'oui',
						],
					'identifiant' => uniqid('@'),
					'saisie'      => 'input',
				],
				[
					'options'     =>
						[
							'nom'              => 'texte',
							'label'            => 'Texte',
							'conteneur_class'  => 'pleine_largeur',
							'rows'             => '10',
							'inserer_barre'    => 'edition',
							'previsualisation' => 'on',
						],
					'identifiant' => uniqid('@'),
					'saisie'      => 'textarea',
				],
			]),
		]
	);

}

function blocks_update_1_0_1() {
	// renommage des tables
	sql_alter('TABLE spip_blocs RENAME TO spip_blocks');
	sql_alter('TABLE spip_blocs_liens RENAME TO spip_blocks_liens');
	sql_alter('TABLE spip_blocs_types RENAME TO spip_blocktypes');

	sql_alter('TABLE spip_blocks CHANGE id_bloc id_block bigint(21) NOT NULL AUTO_INCREMENT');
	sql_alter('TABLE spip_blocks CHANGE id_blocs_type id_blocktype bigint(21) NOT NULL DEFAULT 0');
	sql_alter('TABLE spip_blocks_liens CHANGE id_bloc id_block bigint(21) NOT NULL DEFAULT 0');
	sql_alter('TABLE spip_blocktypes CHANGE id_blocs_type id_blocktype bigint(21) NOT NULL AUTO_INCREMENT');
}

function blocks_update_1_3_0() {
	// transférer les liens de spip_blocks_liens vers spip_blocks
	$blocks_liens = sql_allfetsel('*', 'spip_blocks_liens');
	foreach ($blocks_liens as $blocks_lien) {
		sql_updateq(
			'spip_blocks',
			[
				'id_objet'  => $blocks_lien['id_objet'],
				'objet'     => $blocks_lien['objet'],
				'rang_lien' => $blocks_lien['rang_lien'],
			],
			'id_block = ' . $blocks_lien['id_block']
		);
	}
}
