<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'ajouter_lien_blocktype'                   => 'Ajouter ce type de bloc',

	// C
	'champ_identifiant_label'                  => 'Identifiant',
	'champ_identifiant_explication'            => 'Unique, composé de lettres en minuscules, chiffres ou souligné <code>_</code><br>Cet identifiant servira aussi à trouver le squelette qui affichera ce type de bloc : <code>squelettes/blocks/identifiant.html</code></code>',
	'champ_conteneur_enfants_label'            => 'Ce bloc peut contenir des blocs',
	'champ_blocktypes_enfants_label'           => 'Quels types de blocs peuvent être ajoutés dans ce bloc',
	'champ_conteneur_parents_label'            => 'Ce bloc ne peut être contenu que dans certains blocs',
	'champ_blocktypes_parents_label'           => 'Dans quels types de blocs',
	'champ_saisies_label'                      => 'Paramètres de ce type de bloc',
	'champ_titre_label'                        => 'Titre',
	'champ_description_label'                  => 'Description',
	'champ_objets_label'                       => 'Objets',
	'champ_objets_explication'                 => 'Restreindre ce type de bloc à certains objets',
	'confirmer_supprimer_blocktype'            => 'Confirmez-vous la suppression de cet type de bloc ?',

	// I
	'icone_creer_blocktype'                    => 'Créer un type de bloc',
	'icone_modifier_blocktype'                 => 'Modifier ce type de bloc',
	'icone_modifier_blocktype_champs'          => 'Modifier les champs',
	'info_1_blocktype'                         => 'Un type de bloc',
	'info_aucun_blocktype'                     => 'Aucun type de bloc',
	'info_blocktypes_auteur'                   => 'Les types de blocs de cet auteur',
	'info_nb_blocktypes'                       => '@nb@ types de blocs',
	'info_blocktypes_sans_squelette'           => 'Ce type de bloc n\'a pas de squelette dédié',

	// R
	'retirer_lien_blocktype'                   => 'Retirer ce type de bloc',
	'retirer_tous_liens_blocktypes'            => 'Retirer tous les types de blocs',
	'role_enfant'                              => 'Peut contenir',
	'role_parent'                              => 'Ne peut être contenu que dans',

	// S
	'supprimer_blocktype'                      => 'Supprimer cet type de bloc',

	// T
	'texte_squelette'                          => 'Squelette',
	'texte_squelette_public'                   => 'Squelette public',
	'texte_squelette_prive'                    => 'Squelette privé',
	'texte_ajouter_blocktype'                  => 'Ajouter un type de bloc',
	'texte_changer_statut_blocktype'           => 'Ce type de bloc est :',
	'texte_creer_associer_blocktype'           => 'Créer et associer un type de bloc',
	'texte_definir_comme_traduction_blocktype' => 'Ce type de bloc est une traduction du type de bloc numéro :',
	'titre_blocktype'                          => 'Type de bloc',
	'titre_blocktypes'                         => 'Types de blocs',
	'titre_blocktypes_rubrique'                => 'Types de blocs de la rubrique',
	'titre_langue_blocktype'                   => 'Langue de ce type de bloc',
	'titre_logo_blocktype'                     => 'Logo de ce type de bloc',
	'titre_objets_lies_blocktype'              => 'Liés à ce type de bloc',
	'titre_page_blocktypes'                    => 'Les types de blocs',
];
