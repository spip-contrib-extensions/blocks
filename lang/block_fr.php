<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'ajouter_lien_block'                   => 'Ajouter ce bloc',

	// B
	'bouton_ajouter'                       => 'Ajouter ce bloc',
	'bouton_choisir'                       => 'Choisir ce type de bloc',
	'bouton_enregistrer'                   => 'Enregistrer ce bloc',

	// C
	'champ_ancre_label'                    => 'Ancre nommée (optionnel)',
	'champ_ancre_explication'              => 'Pour pouvoir faire un lien vers ce bloc',
	'champ_block_config_label'             => 'Configuration du bloc',
	'champ_block_contenu_label'            => 'Contenu du bloc',
	'champ_id_blocktype_label'             => 'Type de bloc',
	'champ_valeurs_label'                  => 'Valeurs',
	'confirmer_supprimer_block'            => 'Confirmez-vous la suppression de ce bloc ?',

	// I
	'icone_creer_block'                    => 'Créer un bloc',
	'icone_deplacer_block'                 => 'Déplacer ce bloc',
	'icone_modifier_block'                 => 'Modifier ce bloc',
	'info_1_block'                         => 'Un bloc',
	'info_aucun_block'                     => 'Aucun bloc',
	'info_blocks_auteur'                   => 'Les blocs de cet auteur',
	'info_nb_blocks'                       => '@nb@ blocs',
	'info_objet_lie'                       => 'Objet lié',

	// R
	'retirer_lien_block'                   => 'Retirer ce bloc',
	'retirer_tous_liens_blocks'            => 'Retirer tous les blocs',

	// S
	'supprimer_block'                      => 'Supprimer ce bloc',

	// T
	'texte_ajouter_block'                  => 'Ajouter un bloc',
	'texte_modifier_block'                 => 'Modifier un bloc',
	'texte_changer_statut_block'           => 'Ce bloc est :',
	'texte_creer_associer_block'           => 'Créer et associer un bloc',
	'texte_definir_comme_traduction_block' => 'Ce bloc est une traduction du bloc numéro :',
	'titre_block'                          => 'Bloc',
	'titre_blocks'                         => 'Blocs',
	'titre_blocks_rubrique'                => 'Blocs de la rubrique',
	'titre_langue_block'                   => 'Langue de ce bloc',
	'titre_logo_block'                     => 'Logo de ce bloc',
	'titre_objets_lies_block'              => 'Liés à ce bloc',
	'titre_page_blocks'                    => 'Les blocs',
];
