<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// B
	'blocks_titre'                 => 'Blocks',

	// C
	'cfg_titre_parametrages'       => 'Paramétrages',
	'cfg_objets_explication'       => 'Choisir les objets éditoriaux sur lesquels des blocs pourront être liés.',
	'cfg_objets_label'             => 'Objets éditoriaux liés',

	// T
	'titre_page_configurer_blocks' => 'Configurer Blocks',
];
