<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Blocks
 * @copyright  2023
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Blocks\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function blocks_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['blocks'] = 'blocks';
	$interfaces['table_des_tables']['blocktypes'] = 'blocktypes';

	return $interfaces;
}

/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function blocks_declarer_tables_objets_sql($tables) {

	$tables['spip_blocks'] = [
		'type'                    => 'block',
		'principale'              => 'oui',
		'field'                   => [
			'id_block'     => 'bigint(21) NOT NULL',
			'id_blocktype' => 'bigint(21) NOT NULL DEFAULT 0',
			'objet'        => 'varchar(25) DEFAULT "" NOT NULL',
			'id_objet'     => 'bigint(21) DEFAULT "0" NOT NULL',
			'rang_lien'    => 'int(4) DEFAULT "0" NOT NULL',
			'ancre'        => 'varchar(20) NOT NULL DEFAULT ""',
			'valeurs'      => 'text NOT NULL DEFAULT ""',
			'date'         => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'statut'       => 'varchar(20)  DEFAULT "0" NOT NULL',
			'maj'          => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
		],
		'key'                     => [
			'PRIMARY KEY'      => 'id_block',
			'KEY id_blocktype' => 'id_blocktype',
			'KEY objet'        => 'objet',
			'KEY id_objet'     => 'id_objet',
			'KEY statut'       => 'statut',
		],
		'titre'                   => 'id_blocktype AS titre',
		'date'                    => 'date',
		'champs_editables'        => ['id_blocktype', 'id_objet', 'objet', 'ancre'],
		'champs_versionnes'       => ['id_blocktype', 'id_objet', 'objet', 'ancre', 'valeurs'],
		'tables_jointures'        => ['spip_blocktypes'],
		'statut_textes_instituer' => [
			'prepa'  => 'texte_statut_en_cours_redaction',
			'prop'   => 'texte_statut_propose_evaluation',
			'publie' => 'texte_statut_publie',
			//			'refuse'   => 'texte_statut_refuse',
			//			'poubelle' => 'texte_statut_poubelle',
		],
		'statut'                  => [
			[
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => ['statut', 'tout'],
			],
		],
		'texte_changer_statut'    => 'block:texte_changer_statut_block',
		'page'                    => false,
	];

	$tables['spip_blocktypes'] = [
		'type'              => 'blocktype',
		'principale'        => 'oui',
		'field'             => [
			'id_blocktype' => 'bigint(21) NOT NULL',
			'titre'        => 'text NOT NULL DEFAULT ""',
			'description'  => 'text NOT NULL DEFAULT ""',
			'identifiant'  => 'varchar(255) NOT NULL DEFAULT ""',
			'saisies'      => 'text NOT NULL DEFAULT ""',
			'objets'       => 'text NOT NULL DEFAULT ""',
			'maj'          => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
		],
		'key'               => [
			'PRIMARY KEY'            => 'id_blocktype',
			'UNIQUE KEY identifiant' => 'identifiant',
		],
		'titre'             => 'titre AS titre, "" AS lang',
		'champs_editables'  => ['titre', 'description', 'saisies', 'objets', 'identifiant'],
		'champs_versionnes' => ['titre', 'description', 'saisies', 'objets', 'identifiant'],
		'rechercher_champs' => ["titre" => 10, 'description' => 5, 'identifiant' => 5],
		'tables_jointures'  => [],
		'roles_titres'      => [
			'enfant' => _T('blocktype:role_enfant'),
			'parent' => _T('blocktype:role_parent'),
		],
		'roles_objets'      => [
			'blocktypes' => [
				'choix'  => ['enfant', 'parent'],
				'defaut' => '',
			],
		],
		'page'              => false,
	];

	// jointure potentielle avec tous les objets
	$tables[]['tables_jointures'][] = 'blocks';

	return $tables;
}

/**
 * Déclaration des tables secondaires (liaisons)
 *
 * @pipeline declarer_tables_auxiliaires
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function blocks_declarer_tables_auxiliaires($tables) {

	// table de liaison entre blocktypes enfants / parents
	$tables['spip_blocktypes_liens'] = [
		'field' => [
			'id_blocktype' => 'bigint(21) DEFAULT "0" NOT NULL',
			'id_objet'     => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet'        => 'varchar(25) DEFAULT "" NOT NULL',
			'rang_lien'    => 'int(4) DEFAULT "0" NOT NULL',
			'role'         => 'varchar(25) DEFAULT "" NOT NULL', // [enfant|parent]
		],
		'key'   => [
			'PRIMARY KEY'      => 'id_blocktype,id_objet,objet,role',
			'KEY id_blocktype' => 'id_blocktype',
		],
	];

	return $tables;
}
