<?php
/**
 * Fonctions utiles au plugin Blocks
 *
 * @plugin     Blocks
 * @copyright  2023
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Blocks\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Sérialisation de données (saisies ou valeurs)
 *
 * @param $data
 * @return false|string
 */
function blocks_serialize($data) {
	return ($data && is_array($data)) ? json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK) : '';
}

/**
 * Désérialisation de données (saisies ou valeurs)
 *
 * @param $data
 * @return array|null
 */
function blocks_deserialize($data) {
	$retour = json_decode($data ?? '', true);
	return is_array($retour) ? $retour : null;
}

/**
 * Compile la balise `#GENERER_BLOCK` qui génère l'affiche d'un block'
 *
 * @param Champ $p Pile au niveau de la balise
 * @return Champ   Pile complétée par le code à générer
 * @uses _block_charger_block()
 * @balise
 * @example
 *                 ```
 *                 #GENERER_BLOCK génère le block de la boucle(BLOCK) en cours
 *                 #GENERER_BLOCK{3} génère le block 3
 *                 ```
 */
function balise_GENERER_BLOCK_dist($p) {
	if (!($id_block = interprete_argument_balise(1, $p))) {
		if ($p->id_boucle) {
			$id_block = champ_sql($p->boucles[$p->id_boucle]->primary, $p);
		}
	}

	if (!$id_block) {
		$msg = _T('zbug_balise_sans_argument', ['balise' => ' GENERER_BLOCK']);
		erreur_squelette($msg, $p);
		$p->interdire_scripts = true;
		return $p;
	}

	$p->code = "_block_charger_block($id_block)";
	$p->interdire_scripts = false;

	return $p;
}

/**
 * Fonction interne à la balise GENERER_BLOCK
 *
 * @param int $id_block
 * @return string
 */
function _block_charger_block(int $id_block): string {
	$html = '';
	$where = ['b.id_block = ' . (int)$id_block];
	if (!test_espace_prive()) {
		$where[] = 'b.statut = ' . sql_quote('publie');
	}
	if ($infos = sql_fetsel(
		'b.id_blocktype, b.valeurs, bt.saisies, bt.identifiant',
		'spip_blocktypes bt join spip_blocks b using(id_blocktype)',
		$where
	)) {
		$contexte = array_merge(
			[
				'id_block' => $id_block,
			],
			block_get_valeurs($infos['saisies'], $infos['valeurs'])
		);

		$html = recuperer_fond('inclure/block', $contexte);

		$where = [
			'b.objet="block"',
			'b.id_objet = ' . $id_block,
		];
		if (!test_espace_prive()) {
			$where[] = 'b.statut = ' . sql_quote('publie');
		}
		if ($blocs_enfants = sql_allfetsel(
			'b.id_block, bt.saisies, b.valeurs',
			'spip_blocks b join spip_blocktypes bt using(id_blocktype)',
			$where,
			'', 'b.rang_lien'
		)) {
			$html_enfants = '';
			$idsortable = '';
			$html_wrap = '';
			if (test_espace_prive()) {
				$idsortable = uniqid();
				$html_wrap = '<div class="objet_blocks" data-lien="block/' . $id_block . '">';
			}

			foreach ($blocs_enfants as $bloc_enfant) {
				$contexte = array_merge(
					[
						'id_block'            => $bloc_enfant['id_block'],
						'objet'               => 'block',
						'id_objet'            => $id_block,
						'id_blocktype_parent' => $infos['id_blocktype'],
						'idsortable'          => $idsortable,
					],
					block_get_valeurs($bloc_enfant['saisies'], $bloc_enfant['valeurs'])
				);
				if (test_espace_prive()) {
					$squelette = 'prive/squelettes/inclure/block_objet';
					$recuperer_fond_options = ['ajax' => 'block_' . $bloc_enfant['id_block'] . '_block_' . $id_block];
				} else {
					$squelette = 'inclure/block';
					$recuperer_fond_options = [];
				}
				$html_enfants .= recuperer_fond(
					$squelette,
					$contexte,
					$recuperer_fond_options
				);
			}

			if (str_contains($html, '<!--blocks-->')) {
				$html = str_replace('<!--blocks-->', $html_enfants, $html);
			} else {
				$html .= $html_enfants;
			}
			if ($html_wrap) {
				$html = wrap($html, $html_wrap);
			}
		}

		// Ajouter un block
		if (test_espace_prive() && bloctypes_trouver_enfants($infos['id_blocktype'])) {
			$html .= recuperer_fond(
				'prive/squelettes/inclure/block_objet',
				[
					'objet'               => 'block',
					'id_objet'            => $id_block,
					//						'id_block'            => 'new',
					'id_blocktype_parent' => $infos['id_blocktype'],
					'edit_bloc'           => 1,
				],
				[
					'ajax' => 'block_new_block_' . $id_block,
				]
			);
		}

	}

	return $html;
}

/**
 * Compile la balise `#GENERER_BLOCKS` qui génère l'affiche des blocks liés à un objet
 *
 * @param Champ $p Pile au niveau de la balise
 * @return Champ   Pile complétée par le code à générer
 * @uses _block_charger_blocks()
 * @balise
 * @example
 *                 ```
 *                 #GENERER_BLOCKS génère les blocks de l'objet de la bouvle en cours
 *                 #GENERER_BLOCKS{article,3} génère les blocks de l'article 3
 *                 ```
 */
function balise_GENERER_BLOCKS_dist($p) {
	if ($objet = interprete_argument_balise(1, $p)) {
		$id_objet = interprete_argument_balise(2, $p);
	} else {
		$id_objet = null;
		if ($p->id_boucle) {
			$id_objet = champ_sql($p->boucles[$p->id_boucle]->primary, $p);
			$objet = "objet_type('" . $p->boucles[$p->id_boucle]->id_table . "')";
		}
	}

	if (!$objet || !$id_objet) {
		$msg = _T('zbug_balise_sans_argument', ['balise' => ' GENERER_BLOCKS']);
		erreur_squelette($msg, $p);
		$p->interdire_scripts = true;
		return $p;
	}

	$p->code = "_block_charger_blocks($objet, $id_objet)";
	$p->interdire_scripts = false;

	return $p;
}

/**
 * Fonction interne à la balise GENERER_BLOCKS
 *
 * @param string $objet
 * @param int    $id_objet
 * @return string
 */
function _block_charger_blocks(string $objet, int $id_objet): string {
	$retour = '';
	$where = [
		'objet = ' . sql_quote($objet),
		'id_objet = ' . (int)$id_objet,
	];
	if (!test_espace_prive()) {
		$where[] = 'statut = ' . sql_quote('publie');
	}
	$blocks = sql_allfetsel(
		'id_block',
		'spip_blocks b',
		$where,
		'',
		'rang_lien'
	);
	foreach ($blocks as $block) {
		$retour .= _block_charger_block($block['id_block']);
	}

	return $retour;
}

/**
 * Générer le titre d'un block
 * composé de son type et du titre de l'objet auquel il est lié
 *
 * @param $id_block
 * @return string
 */
function generer_TITRE_BLOCK($id_block): string {
	if ($infos = sql_fetsel(
		'bt.titre, b.objet, b.rang_lien, b.id_objet',
		'spip_blocks b join spip_blocktypes bt using(id_blocktype)',
		'b.id_block = ' . (int)$id_block
	)) {
		$titre = $infos['titre'];
		if ($infos['id_objet'] && $infos['objet']) {
			$titre = generer_objet_info($infos['id_objet'], $infos['objet'], 'titre') . ' - ' . $titre . ' #' . $infos['rang_lien'];
		}
	} else {
		$titre = _T('block:titre_block') . ' ' . $id_block;
	}

	return $titre;
}

/**
 * Retourner un tableau des valeurs saisies
 *
 * @param string $saisies (sérialisées)
 * @param string $valeurs (sérialisées)
 *
 * @return array
 */
function block_get_valeurs(string $saisies, string $valeurs): array {
	$retour = [];
	$saisies_blocktype = blocks_deserialize($saisies);
	if ($valeurs_saisies = blocks_deserialize($valeurs)) {
		include_spip('inc/saisies_lister');
		$saisies_par_nom = saisies_lister_par_nom($saisies_blocktype);
		foreach ($saisies_par_nom as $nom => $saisie) {
			$retour[$nom] = $valeurs_saisies[$nom] ?? null;
		}
	}
	return $retour;
}

/**
 * Chercher le squelette correspondant à un block par son identifiant
 *
 * @param string $identifiant    Identifiant du block
 * @param bool   $force_public   Forcer une recherche du squelette public
 * @param bool   $chemin_complet Retourne le chemin complet du squelette
 * @return string Chemin du squelette trouvé
 */
function blocks_trouver_squelette($identifiant, $force_public = false, $chemin_complet = false, $dist = true): string {
	static $cache;
	$cle = $identifiant . (int)$force_public . (int)$chemin_complet . (int)$dist;

	if (isset($cache[$cle])) {
		return $cache[$cle];
	}

	$squelette = '';
	if (
		!$force_public
		&& test_espace_prive()
		&& ($f = find_in_path($identifiant . '.' . _EXTENSION_SQUELETTES, 'blocks_prive/'))
		&& lire_fichier($f, $contenu)
	) {
		$squelette = $chemin_complet ? $f : 'blocks_prive/' . $identifiant;
	} else if (
		($f = find_in_path($identifiant . '.' . _EXTENSION_SQUELETTES, 'blocks/'))
		&& lire_fichier($f, $contenu)
	) {
		$squelette = $chemin_complet ? $f : 'blocks/' . $identifiant;
	} else if (
		$dist
		&& ($f = find_in_path('dist.' . _EXTENSION_SQUELETTES, 'blocks/'))
		&& lire_fichier($f, $contenu)
	) {
		$squelette = $chemin_complet ? $f : 'blocks/dist';
	}
	$cache[$cle] = $squelette;

	return $squelette;
}

/**
 * Calculer la liste des types de blocks pouvant être créés sous un type de block
 * @param $id_blocktype
 * @return array
 */
function bloctypes_trouver_enfants($id_blocktype): array {
	return bloctypes_trouver_role($id_blocktype, 'enfant');
}

/**
 * Calculer la liste des types de blocks dans lesquels un type de block peut être créé
 * @param $id_blocktype
 * @return array
 */
function bloctypes_trouver_parents($id_blocktype): array {
	return bloctypes_trouver_role($id_blocktype, 'parent');
}

/**
 * Calculer une liste de types de blocks enfants ou parents en fonction du rôle
 * @param $id_blocktype
 * @param $role
 * @return array
 */
function bloctypes_trouver_role($id_blocktype, $role): array {
	include_spip('action/editer_liens');
	if (
		$id_blocktype
		&& $role
		&& ($blocktypes_parents = objet_trouver_liens(['blocktype' => $id_blocktype], ['blocktype' => '*'], ['role' => $role]))
	) {
		return array_column($blocktypes_parents, 'id_objet');
	}
	return [];
}
