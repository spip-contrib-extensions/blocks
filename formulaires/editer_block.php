<?php
/**
 * Gestion du formulaire de d'édition de block
 *
 * @plugin     Blocks
 * @copyright  2023
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Blocks\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('blocks_fonctions');
include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string  $id_block            Identifiant du block. 'new' pour un nouveau block.
 * @param string|null $objet               Type d'objet à associer (article, rubrique,...)
 * @param int|null    $id_objet            Id de l'objet à associer
 * @param int|null    $id_blocktype_parent Type du block parent
 * @param string|null $retour              URL de redirection après le traitement
 * @param string|null $include             Indique si le formulaire est ouvert en inclusion
 * @return string              Hash du formulaire
 */
function formulaires_editer_block_identifier_dist($id_block = 'new', $objet = null, $id_objet = null, $id_blocktype_parent = null, $retour = null, $include = null) {
	return serialize([intval($id_block), $objet, $id_objet]);
}

/**
 * Chargement du formulaire d'édition de block
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @param int|string  $id_block            Identifiant du block. 'new' pour un nouveau block.
 * @param string|null $objet               Type d'objet à associer (article, rubrique,...)
 * @param int|null    $id_objet            Id de l'objet à associer
 * @param int|null    $id_blocktype_parent Type du block parent
 * @param string|null $retour              URL de redirection après le traitement
 * @param string|null $include             Indique si le formulaire est ouvert en inclusion
 * @return array               Environnement du formulaire
 * @uses formulaires_editer_objet_charger()
 */
function formulaires_editer_block_charger_dist($id_block = 'new', $objet = null, $id_objet = null, $id_blocktype_parent = null, $retour = null, $include = null) {
	if ((int)$id_block) {
		$id_blocktype = (int)sql_getfetsel('id_blocktype', 'spip_blocks', 'id_block = ' . $id_block);
		$infos = sql_fetsel('id_objet, objet', 'spip_blocks', 'id_block = ' . $id_block);
		$objet = $infos['objet'];
		$id_objet = $infos['id_objet'];
		$valeurs = formulaires_editer_objet_charger('block', $id_block, $id_blocktype, 0, $retour, '');
	} else {
		$id_blocktype = _request('id_blocktype');
	}
	if (!$objet || !(int)$id_objet || !autoriser('modifier', $objet, $id_objet)) {
		return null;
	}

	$valeurs['objet'] = $objet;
	$valeurs['id_objet'] = $id_objet;
	$valeurs['include'] = (bool)$include;
	$valeurs['redirect'] = $retour;

	if ($id_blocktype) {
		$valeurs['deplie_block'] = true;
		// charger les saisies du type de block
		$saisies_blocktype = [];
		if ($saisies_json = sql_getfetsel('saisies', 'spip_blocktypes', 'id_blocktype = ' . $id_blocktype)) {
			$saisies_blocktype = blocks_deserialize($saisies_json);
		}

		// ajout du champ "ancre nommée"
		$saisies_blocktype[] = [
			'saisie'  => 'input',
			'options' => [
				'nom'         => 'ancre',
				'label'       => _T('block:champ_ancre_label'),
				'explication' => _T('block:champ_ancre_explication'),
			],
		];

		// passer les saisies au formulaire
		$valeurs['_saisies'] = saisies_inserer(
			$saisies_blocktype,
			[
				'saisie'  => 'hidden',
				'options' => [
					'nom'         => 'id_blocktype',
					'obligatoire' => 'oui',
				],
			],
			0
		);
		$valeurs['id_blocktype'] = $id_blocktype;

		// charger les valeurs déjà saisies
		$valeurs_saisies = blocks_deserialize($valeurs['valeurs'] ?? null);
		$valeurs_saisies['ancre'] = $valeurs['ancre'] ?? null;
		include_spip('inc/saisies_lister');
		$saisies_par_nom = saisies_lister_par_nom($saisies_blocktype);
		foreach ($saisies_par_nom as $nom => $saisie) {
			if ((int)$id_block) {
				$valeurs[$nom] = (_request($nom) ? _request($nom) : ($valeurs_saisies[$nom] ?? ''));
			} else {
				$valeurs[$nom] = '';
			}
		}

	} else {

		$valeurs['_saisies'] = [
			[
				'saisie'  => 'blocktypes',
				'options' => [
					'nom'                 => 'id_blocktype',
					'obligatoire'         => 'oui',
					'label'               => _T('block:champ_id_blocktype_label'),
					'objet'               => $objet,
					'id_blocktype_parent' => $id_blocktype_parent,
				],
			],
		];

	}
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de block
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @param int|string  $id_block            Identifiant du block. 'new' pour un nouveau block.
 * @param string|null $objet               Type d'objet à associer (article, rubrique,...)
 * @param int|null    $id_objet            Id de l'objet à associer
 * @param int|null    $id_blocktype_parent Type du block parent
 * @param string|null $retour              URL de redirection après le traitement
 * @param string|null $include             Indique si le formulaire est ouvert en inclusion
 * @return array               Tableau des erreurs
 * @uses formulaires_editer_objet_verifier()
 */
function formulaires_editer_block_verifier_dist($id_block = 'new', $objet = null, $id_objet = null, $id_blocktype_parent = null, $retour = null, $include = null) {
	if ((int)$id_block) {
		$id_blocktype = (int)sql_getfetsel('id_blocktype', 'spip_blocks', 'id_block = ' . $id_block);
	} else {
		$id_blocktype = _request('id_blocktype');
	}
	$erreurs = [];

	if (_request('choisir') && !$id_blocktype) {
		$erreurs['id_blocktype'] = _T('info_obligatoire');
	}

	if (_request('ajouter')) {
		if ($id_blocktype) {
			// charger les saisies du type de block
			$saisies_blocktype = [];
			if ($saisies_json = sql_getfetsel('saisies', 'spip_blocktypes', 'id_blocktype = ' . $id_blocktype)) {
				$saisies_blocktype = blocks_deserialize($saisies_json);
			}
			$erreurs = saisies_verifier($saisies_blocktype);
		} else {
			$erreurs['id_blocktype'] = _T('info_obligatoire');
		}
	}

	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de block
 *
 * Traiter les champs postés
 *
 * @param int|string  $id_block            Identifiant du block. 'new' pour un nouveau block.
 * @param string|null $objet               Type d'objet à associer (article, rubrique,...)
 * @param int|null    $id_objet            Id de l'objet à associer
 * @param int|null    $id_blocktype_parent Type du block parent
 * @param string|null $retour              URL de redirection après le traitement
 * @param string|null $include             Indique si le formulaire est ouvert en inclusion
 * @return array                Retours des traitements
 * @uses formulaires_editer_objet_traiter()
 */
function formulaires_editer_block_traiter_dist($id_block = 'new', $objet = null, $id_objet = null, $id_blocktype_parent = null, $retour = null, $include = null) {
	if ((int)$id_block) {
		$id_blocktype = (int)sql_getfetsel('id_blocktype', 'spip_blocks', 'id_block = ' . $id_block);
		$infos = sql_fetsel('id_objet, objet', 'spip_blocks', 'id_block = ' . $id_block);
		$objet = $infos['objet'];
		$id_objet = $infos['id_objet'];
		$creation = false;
	} else {
		$id_blocktype = _request('id_blocktype');
		$creation = true;
	}
	$retours = [];

	if ($id_blocktype && $objet && (int)$id_objet && _request('ajouter')) {

		$retours = formulaires_editer_objet_traiter('block', $id_block, $id_blocktype, 0, $retour, '');

		if ($id_block = $retours['id_block']) {

			// mettre à jour le block avec les valeurs saisies
			$saisies_blocktype = [];
			if ($saisies_json = sql_getfetsel('saisies', 'spip_blocktypes', 'id_blocktype = ' . $id_blocktype)) {
				$saisies_blocktype = blocks_deserialize($saisies_json);
			}
			include_spip('inc/saisies_lister');
			$valeurs = [];
			$saisies_par_nom = saisies_lister_par_nom($saisies_blocktype);
			foreach ($saisies_par_nom as $nom => $saisie) {
				$valeurs[$nom] = _request($nom);
			}
			sql_updateq('spip_blocks', ['valeurs' => blocks_serialize($valeurs)], 'id_block = ' . $id_block);

			// associer à l'objet
			if ($creation) {
				$rang_max = sql_getfetsel(
					'max(rang_lien)',
					'spip_blocks',
					[
						'objet = ' . sql_quote($objet),
						'id_objet = ' . $id_objet,
					]
				);
				sql_updateq(
					'spip_blocks',
					[
						'id_objet'  => $id_objet,
						'objet'     => $objet,
						'rang_lien' => $rang_max + 1,
					],
					'id_block = ' . $id_block
				);
			}

			// un block est automatiquement publié lors de sa création
			if ($creation) {
				objet_instituer('block', $id_block, ['statut' => 'publie']);
			}

		}
	}

	if(_request('ajax_id')) {
		return ['message_ok' => '<script type="text/javascript">ajaxReload("'._request('ajax_id').'",{args:{edit_bloc:""}});</script>'];
	}

	return $retours;
}
