<?php
/**
 * Gestion du formulaire de d'édition de blocktype
 *
 * @plugin     Blocks
 * @copyright  2023
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Blocks\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');
include_spip('action/editer_liens');
include_spip('blocks_fonctions');

/**
 * Déclaration des saisies de blocktype
 *
 * @param int|string $id_blocktype
 *     Identifiant du blocktype. 'new' pour un nouveau blocktype.
 * @param string     $retour
 *     URL de redirection après le traitement
 * @param int        $lier_trad
 *     Identifiant éventuel d'un blocktype source d'une traduction
 * @param string     $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row
 *     Valeurs de la ligne SQL du blocktype, si connu
 * @param string     $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array[]
 *     Saisies du formulaire
 */
function formulaires_editer_blocktype_saisies_dist($id_blocktype = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$saisies = [
		[
			'saisie'  => 'input',
			'options' => [
				'nom'         => 'titre',
				'obligatoire' => 'oui',
				'label'       => _T('blocktype:champ_titre_label'),

			],
		],
		[
			'saisie'  => 'textarea',
			'options' => [
				'nom'   => 'description',
				'label' => _T('blocktype:champ_description_label'),
				'rows'  => 5,

			],
		],
		[
			'saisie'  => 'case',
			'options' => [
				'nom'        => 'conteneur_enfants',
				'label_case' => _T('blocktype:champ_conteneur_enfants_label'),
			],
		],
		[
			'saisie'  => 'blocktypes',
			'options' => [
				'nom'         => 'blocktypes_enfants',
				'label'       => _T('blocktype:champ_blocktypes_enfants_label'),
				'afficher_si' => '@conteneur_enfants@ == "on"',
				'multiple'    => 'oui',
				'titre_court' => 'oui',
				'exclus'      => [$id_blocktype],
			],
		],
		[
			'saisie'  => 'case',
			'options' => [
				'nom'        => 'conteneur_parents',
				'label_case' => _T('blocktype:champ_conteneur_parents_label'),
			],
		],
		[
			'saisie'  => 'blocktypes',
			'options' => [
				'nom'         => 'blocktypes_parents',
				'label'       => _T('blocktype:champ_blocktypes_parents_label'),
				'afficher_si' => '@conteneur_parents@ == "on"',
				'multiple'    => 'oui',
				'titre_court' => 'oui',
				'exclus'      => [$id_blocktype],
			],
		],
		[
			'saisie'  => 'input',
			'options' => [
				'nom'         => 'identifiant',
				'obligatoire' => 'oui',
				'label'       => _T('blocktype:champ_identifiant_label'),
				'explication' => _T('blocktype:champ_identifiant_explication'),
			],
		],
	];

	if (count(array_filter(lire_config('blocks/objets', []))) > 1) {
		$saisies[] = [
			'saisie'  => 'blocks_objets',
			'options' => [
				'nom'         => 'objets',
				'label'       => _T('blocktype:champ_objets_label'),
				'explication' => _T('blocktype:champ_objets_explication'),
				'multiple'    => 'oui',
			],
		];
	}

	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_blocktype
 *     Identifiant du blocktype. 'new' pour un nouveau blocktype.
 * @param string     $retour
 *     URL de redirection après le traitement
 * @param int        $lier_trad
 *     Identifiant éventuel d'un blocktype source d'une traduction
 * @param string     $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row
 *     Valeurs de la ligne SQL du blocktype, si connu
 * @param string     $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_blocktype_identifier_dist($id_blocktype = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return serialize([intval($id_blocktype)]);
}

/**
 * Chargement du formulaire d'édition de blocktype
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @param int|string $id_blocktype
 *     Identifiant du blocktype. 'new' pour un nouveau blocktype.
 * @param string     $retour
 *     URL de redirection après le traitement
 * @param int        $lier_trad
 *     Identifiant éventuel d'un blocktype source d'une traduction
 * @param string     $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row
 *     Valeurs de la ligne SQL du blocktype, si connu
 * @param string     $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 * @uses formulaires_editer_objet_charger()
 *
 */
function formulaires_editer_blocktype_charger_dist($id_blocktype = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('blocktype', $id_blocktype, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
	$valeurs['objets'] = blocks_deserialize($valeurs['objets']);

	if ($valeurs['blocktypes_enfants'] = bloctypes_trouver_enfants($id_blocktype)) {
		$valeurs['conteneur_enfants'] = 'on';
	}

	if ($valeurs['blocktypes_parents'] = bloctypes_trouver_parents($id_blocktype)) {
		$valeurs['conteneur_parents'] = 'on';
	}

	$valeurs['saisies'] = call_user_func_array('formulaires_editer_blocktype_saisies_dist', func_get_args());
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de blocktype
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @param int|string $id_blocktype
 *     Identifiant du blocktype. 'new' pour un nouveau blocktype.
 * @param string     $retour
 *     URL de redirection après le traitement
 * @param int        $lier_trad
 *     Identifiant éventuel d'un blocktype source d'une traduction
 * @param string     $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row
 *     Valeurs de la ligne SQL du blocktype, si connu
 * @param string     $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 * @uses formulaires_editer_objet_verifier()
 *
 */
function formulaires_editer_blocktype_verifier_dist($id_blocktype = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return formulaires_editer_objet_verifier('blocktype', $id_blocktype, ['titre', 'identifiant']);
}

/**
 * Traitement du formulaire d'édition de blocktype
 *
 * Traiter les champs postés
 *
 * @param int|string $id_blocktype
 *     Identifiant du blocktype. 'new' pour un nouveau blocktype.
 * @param string     $retour
 *     URL de redirection après le traitement
 * @param int        $lier_trad
 *     Identifiant éventuel d'un blocktype source d'une traduction
 * @param string     $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array      $row
 *     Valeurs de la ligne SQL du blocktype, si connu
 * @param string     $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 * @uses formulaires_editer_objet_traiter()
 *
 */
function formulaires_editer_blocktype_traiter_dist($id_blocktype = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$creation = (int)$id_blocktype ? false : true;
	set_request('objets', blocks_serialize(_request('objets')));
	$retours = formulaires_editer_objet_traiter('blocktype', $id_blocktype, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

	if ($id_blocktype = $retours['id_blocktype']) {

		// mettre à jour les rôles
		objet_dissocier(['blocktype' => $id_blocktype], ['blocktype' => '*'], ['role' => 'enfant']);
		if ($blocktypes_enfants = _request('blocktypes_enfants')) {
			foreach ($blocktypes_enfants as $blocktype_enfant) {
				objet_associer(['blocktype' => $id_blocktype], ['blocktype' => $blocktype_enfant], ['role' => 'enfant']);
			}
		}
		objet_dissocier(['blocktype' => $id_blocktype], ['blocktype' => '*'], ['role' => 'parent']);
		if ($blocktypes_parents = _request('blocktypes_parents')) {
			foreach ($blocktypes_parents as $blocktype_parent) {
				objet_associer(['blocktype' => $id_blocktype], ['blocktype' => $blocktype_parent], ['role' => 'parent']);
				objet_associer(['blocktype' => $blocktype_parent], ['blocktype' => $id_blocktype], ['role' => 'enfant']);
			}
		}

		// réassigner le role enfant pour les roles parents existants
		if ($blocktypes_parents = objet_trouver_liens(['blocktype' => '*'], ['blocktype' => '*'], ['role' => 'parent'])) {
			foreach ($blocktypes_parents as $blocktype_parent) {
				objet_associer(['blocktype' => $blocktype_parent['id_objet']], ['blocktype' => $blocktype_parent['id_blocktype']], ['role' => 'enfant']);
			}
		}

	}

	if ($creation) {
		// en cas de création d'un type de block, rediriger vers la configuration des champs
		$retours['redirect'] = generer_url_ecrire('blocktype_edit_champs', 'id_blocktype=' . $retours['id_blocktype']);
	}

	return $retours;
}
