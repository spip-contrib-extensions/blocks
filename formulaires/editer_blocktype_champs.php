<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('blocks_fonctions');

function formulaires_editer_blocktype_champs_charger($id_blocktype, $redirect = '') {
	$id_blocktype = intval($id_blocktype);
	$contexte = [];
	$contexte['id_blocktype'] = $id_blocktype;

	// On teste si le formulaire existe
	if ($id_blocktype
		and $blocktype = sql_fetsel('*', 'spip_blocktypes', 'id_blocktype = ' . $id_blocktype)
		and autoriser('editer', 'blocktype', $id_blocktype)
	) {
		$saisies = blocks_deserialize($blocktype['saisies']);

		// Est-ce qu'on restaure une révision ?
		if ($id_version = _request('id_version')) {
			include_spip('inc/revisions');
			$old = recuperer_version($id_blocktype, 'blocktype', $id_version);
			$saisies = blocks_deserialize($old['saisies']);
		}
		if (!is_array($saisies)) {
			$saisies = [];
		}
		$contexte['_saisies'] = $saisies;
		$contexte['id'] = $id_blocktype;
		$contexte['saisie_id'] = "blocktype_$id_blocktype";

	}

	return $contexte;
}

function formulaires_editer_blocktype_champs_verifier($id_blocktype, $redirect = '') {
	include_spip('inc/saisies');
	$erreurs = [];

	// Si c'est pas une confirmation ni une annulation, ni un revert
	if (!_request('enregistrer_confirmation')
		and !($annulation = _request('annulation'))
		and !_request('revert')
	) {
		// On récupère le formulaire dans la session
		$saisies_nouvelles = session_get("constructeur_formulaire_blocktype_$id_blocktype");
		$md5_precedent_formulaire_initial = session_get("constructeur_formulaire_blocktype_$id_blocktype" . '_md5_formulaire_initial');

		// On récupère les anciennes saisies
		$saisies_anciennes = sql_getfetsel(
			'saisies',
			'spip_blocktypes',
			'id_blocktype = ' . $id_blocktype
		);
		if (!$saisies_anciennes) {
			return $erreurs;
		}
		// On vérifie que les saisies en bases n'ont pas été modifiés depuis le début de la modification du formulaire
		// Si tel est le cas, on demande de recommencer la modif du formulaire, avec la saisie en base
		// Ne pas le faire si on est en train de restaurer une vieille version, puisque dans ce cas ce qui compte sera bien sur la veille version qu'on veut restaurer, et pas la version plus récente en base:)
		// Attention à s'assurer que tout les elements du tableau soit bien soit des tableaux, soit un string
		// En effet, le md5 du formulaire_initial est calculé à partir de ce qui est passé au squelette
		// Or dès qu'une valeur est passée à un squelette, elle est changé en string, à cause du mode de compilation (?)
		$saisies_anciennes = blocks_deserialize($saisies_anciennes);
		$saisies_anciennes_str = $saisies_anciennes;
		array_walk_recursive($saisies_anciennes_str, 'blocktypes_array_walk_recursive_strval');
		$md5_saisies_anciennes = md5(serialize($saisies_anciennes_str));
		if ($md5_precedent_formulaire_initial and $md5_precedent_formulaire_initial != $md5_saisies_anciennes and !_request('id_version')) {
			session_set("constructeur_formulaire_blocktype_$id_blocktype", $saisies_anciennes);
			session_set("constructeur_formulaire_blocktype_$id_blocktype" . '_md5_formulaire_initial', $md5_saisies_anciennes);
			$erreurs['message_erreur'] = _T('formidable:erreur_saisies_modifiees_parallele');
			$erreurs['saisies_modifiees_parallele'] = _T('formidable:erreur_saisies_modifiees_parallele');
			return $erreurs;
		}

		// On compare les anciennes saisies aux nouvelles
		$comparaison = saisies_comparer($saisies_anciennes, $saisies_nouvelles);

		// S'il y a des suppressions, on demande confirmation avec attention
		if ($comparaison['supprimees']) {
			$erreurs['message_erreur'] = _T('saisies:construire_attention_supprime');
		}

		//On vérifie s'il y a pas d'incohérence dans les afficher_si
		$erreurs_afficher_si = saisies_verifier_coherence_afficher_si($saisies_nouvelles ?? []);
		if ($erreurs_afficher_si) {
			$erreurs['saisies_incoherence_afficher_si'] = true;
			if ($erreurs['message_erreur'] ?? '') {
				$erreurs['message_erreur'] .= '<br />' . $erreurs_afficher_si;
			} else {
				$erreurs['message_erreur'] = $erreurs_afficher_si;
			}
		}
	} else if (isset($annulation) and $annulation) {
		// Si on annule on génère une erreur bidon juste pour réafficher le formulaire
		$erreurs['pouetpouet'] = true;
		$erreurs['message_erreur'] = '';
	}

	return $erreurs;
}

function formulaires_editer_blocktype_champs_traiter($id_blocktype, $redirect = '') {
	include_spip('inc/saisies');
	include_spip('inc/formidable');
	$retours = [];
	$id_blocktype = intval($id_blocktype);

	if (_request('revert')) {
		session_set("constructeur_formulaire_blocktype_$id_blocktype");
		$retours = ['editable' => true];
	}

	if (_request('enregistrer') or _request('enregistrer_confirmation')) {
		// On récupère le formulaire dans la session
		$saisies_nouvelles = session_get("constructeur_formulaire_blocktype_$id_blocktype");

		// On envoie les nouvelles dans la table
		include_spip('action/editer_objet');
		$err = objet_modifier('blocktype', $id_blocktype, ['saisies' => blocks_serialize(saisies_identifier($saisies_nouvelles))]);

		// Si c'est bon on reinitialise les sessions
		if (!$err) {
			session_set("constructeur_formulaire_blocktype_$id_blocktype");
			session_set("constructeur_formulaire_blocktype_$id_blocktype" . '_md5_formulaire_initial');
		}
	}

	if (strncmp($redirect, 'javascript:', 11) == 0) {
		$retours['message_ok'] = ($retours['message_ok'] ?? '') . '<script type="text/javascript">/*<![CDATA[*/' . substr($redirect, 11) . '/*]]>*/</script>';
		$retours['editable'] = true;
	} else {
		$retours['redirect'] = $redirect;
	}

	return $retours;
}

/**
 * Fonction de rappel pour array_walk_recursive
 * pour automatiquement transformer les valeur numérique en strval
 * comme SPIP le fait (mais par quel biais?) lorsqu'on envoie un tableau en environnement
 * @param &$value
 * @param $key
 **/
function blocktypes_array_walk_recursive_strval(&$value, $key) {
	if (!is_array($value)) {
		$value = strval($value);
	}
}
