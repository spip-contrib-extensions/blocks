<?php
/**
 * Utilisations de pipelines par Blocks
 *
 * @plugin     Blocks
 * @copyright  2023
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Blocks\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function blocks_header_prive($flux) {
	$flux .= '<link rel="stylesheet" href="' . timestamp(find_in_path('prive/css/blocks_prive.css')) . '" type="text/css" />';
	$flux .= '<script type="text/javascript" src="' . produire_fond_statique('prive/javascript/gestion_listes_blocks.js') . '"></script>';
	return $flux;
}

/**
 * Afficher la gestion des blocks sur la vue des objets
 *
 * @pipeline afficher_contenu_objet
 * @param array $flux Données du pipeline
 * @return array      Données du pipeline
 **/
function blocks_afficher_contenu_objet($flux) {
	if (
		($objet = $flux['args']['type'])
		&& ($args = $flux['args'] ?? [])
		&& ($id_objet = $args['id_objet'] ?? null)
		&& ($objets = lire_config('blocks/objets'))
		&& in_array(table_objet_sql($objet), $objets)
	) {
		$texte = recuperer_fond(
			'prive/squelettes/inclure/blocks_objet',
			[
				'objet'    => $objet,
				'id_objet' => $id_objet,
			],
			['ajax' => 'blocks_objet']
		);
		if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}
	return $flux;
}

/**
 * Ajouter les objets sur les vues des parents directs
 *
 * @pipeline affiche_enfants
 * @param array $flux Données du pipeline
 * @return array       Données du pipeline
 **/
function blocks_affiche_enfants($flux) {
	if (
		$e = trouver_objet_exec($flux['args']['exec'])
		and $e['edition'] === false
	) {
		$id_objet = $flux['args']['id_objet'];

		if ($e['type'] === 'blocktype') {
			$flux['data'] .= recuperer_fond(
				'prive/objets/liste/blocks',
				[
					'titre'        => _T('block:titre_blocks'),
					'id_blocktype' => $id_objet,
				]
			);

		}
	}
	return $flux;
}

/**
 * Compter les enfants d'un objet
 *
 * @pipeline objets_compte_enfants
 * @param array $flux Données du pipeline
 * @return array       Données du pipeline
 **/
function blocks_objet_compte_enfants($flux) {
	if ($flux['args']['objet'] == 'blocktype' and $id_blocktype = intval($flux['args']['id_objet'])) {
		// juste les publiés ?
		if (array_key_exists('statut', $flux['args']) and ($flux['args']['statut'] == 'publie')) {
			$flux['data']['blocks'] = sql_countsel('spip_blocks', 'id_blocktype= ' . intval($id_blocktype) . " AND (statut = 'publie')");
		} else {
			$flux['data']['blocks'] = sql_countsel('spip_blocks', 'id_blocktype= ' . intval($id_blocktype) . " AND (statut <> 'poubelle')");
		}
	}
	return $flux;
}

/**
 * Optimiser la base de données
 *
 * Supprime les liens orphelins de l'objet vers quelqu'un et de quelqu'un vers l'objet.
 * Supprime les objets à la poubelle.
 *
 * @pipeline optimiser_base_disparus
 * @param array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function blocks_optimiser_base_disparus($flux) {

	include_spip('action/editer_liens');
	$flux['data'] += objet_optimiser_liens(['block' => '*'], '*');

	sql_delete('spip_blocks', "statut='poubelle' AND maj < " . $flux['args']['date']);
	return $flux;
}
