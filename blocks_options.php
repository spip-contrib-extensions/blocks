<?php
/**
 * Options au chargement du plugin Blocks
 *
 * @plugin     Blocks
 * @copyright  2023
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Blocks\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
