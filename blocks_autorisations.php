<?php
/**
 * Définit les autorisations du plugin Blocks
 *
 * @plugin     Blocks
 * @copyright  2023
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Blocks\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser
 */
function blocks_autoriser() {
}

// -----------------
// Objet blocks

/**
 * Autorisation de voir un élément de menu (blocks)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_blocks_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('webmestre', '', '', $qui);
}

/**
 * Autorisation de voir (blocks)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_blocks_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de voir (block)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_block_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de créer (block)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_block_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], ['0minirezo', '1comite']);
}

/**
 * Autorisation de modifier (block)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_block_modifier_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], ['0minirezo', '1comite']);
}

/**
 * Autorisation de supprimer (block)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_block_supprimer_dist($faire, $type, $id, $qui, $opt) {
	return ($qui['statut'] == '0minirezo' and !$qui['restreint'])
		or (blocks_autoriser_statuts('block', $id, $qui, $opt)
			and $auteurs = blocks_auteurs_objet('block', $id) and in_array($qui['id_auteur'], $auteurs));
}

/**
 * Autorisation de créer l'élément (block) dans un blocktypes
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_blocktype_creerblockdans_dist($faire, $type, $id, $qui, $opt) {
	return ($id and autoriser('voir', 'blocktypes', $id) and autoriser('creer', 'block'));
}

/**
 * Autorisation de lier/délier l'élément (blocks)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_associerblocks_dist($faire, $type, $id, $qui, $opt) {
	return ($qui['statut'] == '0minirezo' and !$qui['restreint'])
		or (blocks_autoriser_statuts('block', $id, $qui, $opt)
			and $auteurs = blocks_auteurs_objet('block', $id) and in_array($qui['id_auteur'], $auteurs));
}

/**
 * Autorisation de créer un logo' (block)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_block_iconifier_dist($faire, $type, $id, $qui, $opt) {
	return false;
}

// -----------------
// Objet blocktypes

/**
 * Autorisation de voir un élément de menu (blocktypes)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_blocktypes_menu_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de voir (blocktypes)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_blocktypes_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de voir (blocktype)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_blocktype_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de créer (blocktype)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_blocktype_creer_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('webmestre', '', '', $qui);
}

/**
 * Autorisation de modifier (blocktype)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_blocktype_modifier_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('webmestre', '', '', $qui);
}

/**
 * Autorisation de supprimer (blocktype)
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_blocktype_supprimer_dist($faire, $type, $id, $qui, $opt) {
	if (!sql_countsel('spip_blocks', 'id_blocktype = ' . (int)$id)) {
		return autoriser('webmestre', '', '', $qui);
	}
	return false;
}

/**
 * Lister les auteurs liés à un objet
 *
 * @param int $objet    Type de l'objet
 * @param int $id_objet Identifiant de l'objet
 * @return array        Liste des id_auteur trouvés
 */
function blocks_auteurs_objet($objet, $id_objet) {
	$auteurs = sql_allfetsel('id_auteur', 'spip_auteurs_liens', ['objet = ' . sql_quote($objet), 'id_objet = ' . intval($id_objet)]);
	if (is_array($auteurs)) {
		return array_column($auteurs, 'id_auteur');
	}
	return [];
}

/**
 * Tester si le statut de l'objet autorise des changements, en fonction du statut de l'auteur.
 *
 * @param string $objet    Type d'objet
 * @param int    $id_objet Identifiant de l'objet
 * @param array  $qui      Description de l'auteur demandant l'autorisation
 * @param array  $opt      Options de l'autorisation
 * @return bool             true s'il a le droit, false sinon
 **/
function blocks_autoriser_statuts($objet, $id_objet, $qui, $opt) {
	$statut = sql_getfetsel('statut', table_objet_sql($objet), id_table_objet($objet) . ' = ' . intval($id_objet));
	return (!isset($opt['statut']) or $opt['statut'] !== 'publie')
		and in_array($qui['statut'], ['0minirezo', '1comite'])
		and in_array($statut, ['prop', 'prepa', 'poubelle']);
}
