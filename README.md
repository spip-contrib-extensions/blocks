# Blocks

Un plugin pour composer des pages sous forme de blocs, rangés les uns à la suite des autres (appelés parfois "lames")

**[WIP]** En cours de développement, des choses peuvent bouger de façon significative (cf TODO.md)

Chaque type de bloc a un identifiant unique (slug), qui permet de chercher un squelette dans le path sous la forme blocks/identifiant.html, sinon on prend blocks/dist.html\
Dans le privé, on cherche d'abord blocks_prive/identifiant.html, ce qui permet d'avoir un affichage différent dans l'espace privé du site public (espace plus réduit en largeur, pas les même css ou js chargés)

Dans la configuration de chaque type de block :
- on peut choisir de restreindre un type de bloc à un objet éditorial\
ex : un block "menu des articles de la rubrique", qu'on ne pourrait associer qu'aux rubriques)\
par défaut : aucune restriction
- on peut définir quels types de blocks il peut contenir\
ex : un block "recette" qui pourrait contenir des blocks "ingrédient"\
par défaut : aucun (le block n'est pas un conteneur)
- ou bien au contraire restreindre ce block à ne pouvoir être créé que dans certains types de blocks\
ex : un block "personne" ne pourrait être créé que dans un block "trombinoscope"\
par défaut : aucune restriction

Dans la configuration de chaque block :
- on peut définir une ancre nommée, pour faire un lien direct vers ce bloc

Dans les squelettes du site public, ajouter simplement #GENERER_BLOCKS pour générer tous les blocs d'un objet.\
La balise reconnait le contexte de la boucle en court, sinon on peut utiliser #GENERER_BLOCKS{objet,#ID_OBJET}

Dans le squelette d'un block "parent" (qui peut contenir des blocks), on peut déclarer un tag `<!--blocks-->` qui sera remplacé par les blocs enfants, ce qui permet de les encapsuler dans un markup personnalisé.\
Par exemple, dans `squelettes/blocks/mon_block_conteneur.html` :
```html
<div class="mon_block_conteneur">
  <h2>#ENV{titre}</h2>
  <div class="mon_block_conteneur__enfants">
    <!--blocks-->
  </div>
</div>
```

