# Todo

## blocks

[ ] Utiliser les crayons pour éditer un block côté public (cf branche dev/crayons)
- problème : sérialiser tous les champs dans ['valeurs'] avant d'enregister : tester pipeline pre_edition ? formulaire_verifier ou _traiter ?

[ ] Ne plus gérer de status de blocks mais gérer deux configs de blocks pour un objet, une publiée une prépa
- utiliser une config en session comme le constructeur de formulaire de saisies ?
- pb à résoudre : pouvoir faire une prévisualisation des blocs en cours d'édition sur la page de l'objet

## blocktypes

**Technique**

[ ] Pouvoir restreindre l'utilisation de certains types de blocs à certains objets
- ajouter un critère sur les boucles blocks ou modifier ces boucles en pipeline pour ne remonter que les blocs dont les types sont associables à l'objet en cours

[ ] Si le type de block est associable aux rubriques, pouvoir restreindre son utilisation à une branche
- remarque idem point précédent

[ ] Pouvoir brancher un type de block sur un modèle auto-documenté (html + yaml), ou un blocks/*.yaml spécifique
- continuer à utiliser en parallèle le constructeur de formulaire ? il est quand même très pratique

[ ] Gestion de champs de type fichiers : comment les associer aux blocks ?
- comme des documents liés dont l'id est référencé dans la valeur du champ, en plus d'un lien dans spip_documents_liens ? (hum...)
- avec des rôles dynamiques ? (hum...)

**UX**

[ ] Créer quelques blocks types à l'installation
- Texte simple avec titre optionnel + niveaux de titre
- ...

## Questions

[ ] Saisie blocktypes : utilisation de leurs logos comme illustrations d'exemples de mise en page ?

[ ] Une config pour insérer directement les blocks dans la balise #TEXTE des articles, pour ne pas nécessiter de modification de squelette et pour que TEXTE renvoie tout le temps le contenu complet (texte + block)
- quid des autres objets

[ ] Pouvoir détacher un block d'un objet et le rattacher à un autre objet ?

[ ] CSS : charger (privé et public) un blocks/identifiant.css ou html.css ou scss ?

[ ] Trouver un nom moins générique que blocks ?

